<?php
$gioiTinh = array(
    0 => 'Nam',
    1 => 'Nữ'
);
$khoa = array(
    '' => '',
    'MAT' => 'Khoa học máy tính',
    'KDL' => 'khoa học vật liệu'
);
// error from inputs
$errors = [];
if ($_SERVER['REQUEST_METHOD']=== 'POST') {

    if (!$_POST['name']) {
        $errors[]='Hãy nhập tên.';
    }

    if(!isset($_POST['gender'])) {
        $errors[]='Hãy nhập giới tính.';
    }

    if (!$_POST['faculty']) {
        $errors[]='Hãy nhập Khoa.';
    }

    if (!$_POST['date']) {
        $errors[]='Hãy nhập ngày sinh.';
    }

    if (!$_POST['address']) {
        $errors[]='Hãy nhập địa chỉ.';
    }
}

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="lab04.css">
</head>

<body>
    <div id='backDiv'>
        <form method="POST" style="position: center !important;
    width:80%;
    margin-left: 10%;
    margin-right: 10%;">
        <?php if (!empty($errors)): ?>
            <div id="error">
                <?php foreach ($errors as $error): ?>
                    <div><?php echo $error ?></div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
            <!-- fill the name info  -->
            <div id="nameDiv" class="required-field">
                <label id="label" class="h-100" for="name">Họ và tên </label>
                <input id="input" class="h-100" type="text" name="name" >
            </div>
            <!-- choose genders -->
            <div id="infoDiv">
                <label id="label" class="h-100" for="gender"> Giới tính </label>
                <div id="radioBtn" >
                    <?php
                    for ($i = 0; $i < 2; $i++) {
                    ?>
                        <input id="radioBtn" type="radio" name="gender"  value= "$i"  >
                        <?php echo $gioiTinh[$i] ?>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <!-- fill in faculty info -->
            <div id="infoDiv">
                <label id="label" class="h-100">Phân khoa </label>
                <select id="input" class="h-100" name="faculty">
                    <?php foreach ($khoa  as $i) : ?>
                        <option><?php echo $i ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div id="dateDiv">
                <label id="label" class="h-100" for="birthday">Ngày sinh </label>
                <td height = '40px'>
                    <input type='date' id="input" class="h-100" name="date" data-date="" data-date-format="DD MMMM YYYY" style = 'line-height: 32px; border-color:#ADD8E6'>
                </td>
                <script>
                    $(document).ready(function() {
                        $("#input").datepicker({
                            format: 'dd-mm-yyyy'    
                        });
                    });
                </script>

            </div>
            <div id="addressDiv">
                <label id="label" class="h-100" for="address">Địa Chỉ </label>
                <input id="input" class="h-100" type="text" name="address">
            </div>
            <div id="btnDiv">
                <button class="btn btn-success" id="submitId" >Đăng ký</button>
            </div>
        </form>
    </div>

</body>

</html>